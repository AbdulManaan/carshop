﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OnlineCarShop.Data;
using OnlineCarShop.Models;
using OnlineCarShop.Services;
using Microsoft.AspNetCore.Identity;
using OnlineCarShop.Repositories;
using Microsoft.EntityFrameworkCore.Storage;

namespace OnlineCarShop
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser,IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            
            services.AddMvc();
            /////for creating session and us app.usesesssion
            services.AddDistributedMemoryCache();
            services.AddSession();

            // Add application services.
            services.AddTransient<ICarRepository, CarRepository>();
            services.AddTransient<IPartRepository, PartRepository>();
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IPartsCategoryRepository, PartsCategoryRepository>();
            services.AddTransient<IAdminRepository, AdminRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();

            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();

            services.AddScoped(sp => ShoppingCart.GetCart(sp));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public async void Configure(IApplicationBuilder app, IHostingEnvironment env
            , ILoggerFactory loggerFactory
            , RoleManager<IdentityRole> roleManager
            , UserManager<ApplicationUser> userManager
            , ApplicationDbContext context
            , IServiceProvider serviceProvider
            , IAdminRepository adminRepo)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

        

            app.UseStaticFiles();

            //app.UseSignalR();

            app.UseIdentity();

            app.UseSession();
            // Add external authentication middleware below. To configure them please see https://go.microsoft.com/fwlink/?LinkID=532715

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var _tempcontext = serviceScope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                _tempcontext.Database.Migrate();
            }
            //await DbInitializer.Initialize(context, serviceProvider, adminRepo);
            await CreateAdminRoleAsync(roleManager, userManager);
        }
        private async Task CreateAdminRoleAsync(RoleManager<IdentityRole> _roleManager
            ,UserManager<ApplicationUser> _userManager)
        {
            if (!await _roleManager.RoleExistsAsync("Admin"))
            {
                var role = new IdentityRole("Admin");
                await _roleManager.CreateAsync(role);
                var user = new ApplicationUser { UserName = "admin@default.com", Email = "admin@default.com" };
                var result = await _userManager.CreateAsync(user, "Password@123");
               
                await _userManager.AddToRoleAsync(user, "Admin");
            }



        }
    }
}
