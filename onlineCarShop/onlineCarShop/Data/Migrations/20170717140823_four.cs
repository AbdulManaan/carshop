﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace OnlineCarShop.Data.Migrations
{
    public partial class four : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CarParts_Cars_CarsId",
                table: "CarParts");

            migrationBuilder.DropIndex(
                name: "IX_CarParts_CarsId",
                table: "CarParts");

            migrationBuilder.DropColumn(
                name: "CarsId",
                table: "CarParts");

            migrationBuilder.AddColumn<int>(
                name: "CarId",
                table: "CarParts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PartId",
                table: "CarParts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Parts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parts", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CarParts_CarId",
                table: "CarParts",
                column: "CarId");

            migrationBuilder.CreateIndex(
                name: "IX_CarParts_PartId",
                table: "CarParts",
                column: "PartId");

            migrationBuilder.AddForeignKey(
                name: "FK_CarParts_Cars_CarId",
                table: "CarParts",
                column: "CarId",
                principalTable: "Cars",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CarParts_Parts_PartId",
                table: "CarParts",
                column: "PartId",
                principalTable: "Parts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CarParts_Cars_CarId",
                table: "CarParts");

            migrationBuilder.DropForeignKey(
                name: "FK_CarParts_Parts_PartId",
                table: "CarParts");

            migrationBuilder.DropTable(
                name: "Parts");

            migrationBuilder.DropIndex(
                name: "IX_CarParts_CarId",
                table: "CarParts");

            migrationBuilder.DropIndex(
                name: "IX_CarParts_PartId",
                table: "CarParts");

            migrationBuilder.DropColumn(
                name: "CarId",
                table: "CarParts");

            migrationBuilder.DropColumn(
                name: "PartId",
                table: "CarParts");

            migrationBuilder.AddColumn<int>(
                name: "CarsId",
                table: "CarParts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CarParts_CarsId",
                table: "CarParts",
                column: "CarsId");

            migrationBuilder.AddForeignKey(
                name: "FK_CarParts_Cars_CarsId",
                table: "CarParts",
                column: "CarsId",
                principalTable: "Cars",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
