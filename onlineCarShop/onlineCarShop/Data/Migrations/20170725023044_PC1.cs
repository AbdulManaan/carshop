﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineCarShop.Data.Migrations
{
    public partial class PC1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Parts_Categories_CategoriesId",
                table: "Parts");

            migrationBuilder.DropForeignKey(
                name: "FK_Parts_PartsCategories_PartsCategoriesId",
                table: "Parts");

            migrationBuilder.DropIndex(
                name: "IX_Parts_CategoriesId",
                table: "Parts");

            migrationBuilder.RenameColumn(
                name: "PartsCategoriesId",
                table: "Parts",
                newName: "CategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_Parts_PartsCategoriesId",
                table: "Parts",
                newName: "IX_Parts_CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Parts_PartsCategories_CategoryId",
                table: "Parts",
                column: "CategoryId",
                principalTable: "PartsCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Parts_PartsCategories_CategoryId",
                table: "Parts");

            migrationBuilder.RenameColumn(
                name: "CategoryId",
                table: "Parts",
                newName: "PartsCategoriesId");

            migrationBuilder.RenameIndex(
                name: "IX_Parts_CategoryId",
                table: "Parts",
                newName: "IX_Parts_PartsCategoriesId");

            migrationBuilder.CreateIndex(
                name: "IX_Parts_CategoriesId",
                table: "Parts",
                column: "CategoriesId");

            migrationBuilder.AddForeignKey(
                name: "FK_Parts_Categories_CategoriesId",
                table: "Parts",
                column: "CategoriesId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Parts_PartsCategories_PartsCategoriesId",
                table: "Parts",
                column: "PartsCategoriesId",
                principalTable: "PartsCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
