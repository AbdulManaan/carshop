﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineCarShop.Data.Migrations
{
    public partial class PC7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Parts_PartsCategories_PartsCategoryId",
                table: "Parts");

            migrationBuilder.DropIndex(
                name: "IX_Parts_PartsCategoryId",
                table: "Parts");

            migrationBuilder.DropColumn(
                name: "PartsCategoryId",
                table: "Parts");

            migrationBuilder.RenameColumn(
                name: "CategoriesId",
                table: "Parts",
                newName: "PartsCategoriesId");

            migrationBuilder.CreateIndex(
                name: "IX_Parts_PartsCategoriesId",
                table: "Parts",
                column: "PartsCategoriesId");

            migrationBuilder.AddForeignKey(
                name: "FK_Parts_PartsCategories_PartsCategoriesId",
                table: "Parts",
                column: "PartsCategoriesId",
                principalTable: "PartsCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Parts_PartsCategories_PartsCategoriesId",
                table: "Parts");

            migrationBuilder.DropIndex(
                name: "IX_Parts_PartsCategoriesId",
                table: "Parts");

            migrationBuilder.RenameColumn(
                name: "PartsCategoriesId",
                table: "Parts",
                newName: "CategoriesId");

            migrationBuilder.AddColumn<int>(
                name: "PartsCategoryId",
                table: "Parts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Parts_PartsCategoryId",
                table: "Parts",
                column: "PartsCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Parts_PartsCategories_PartsCategoryId",
                table: "Parts",
                column: "PartsCategoryId",
                principalTable: "PartsCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
