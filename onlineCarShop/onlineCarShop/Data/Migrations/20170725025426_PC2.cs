﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineCarShop.Data.Migrations
{
    public partial class PC2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Parts_PartsCategories_CategoryId",
                table: "Parts");

            migrationBuilder.RenameColumn(
                name: "CategoryId",
                table: "Parts",
                newName: "PartsCategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_Parts_CategoryId",
                table: "Parts",
                newName: "IX_Parts_PartsCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Parts_PartsCategories_PartsCategoryId",
                table: "Parts",
                column: "PartsCategoryId",
                principalTable: "PartsCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Parts_PartsCategories_PartsCategoryId",
                table: "Parts");

            migrationBuilder.RenameColumn(
                name: "PartsCategoryId",
                table: "Parts",
                newName: "CategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_Parts_PartsCategoryId",
                table: "Parts",
                newName: "IX_Parts_CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Parts_PartsCategories_CategoryId",
                table: "Parts",
                column: "CategoryId",
                principalTable: "PartsCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
