﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineCarShop.Data.Migrations
{
    public partial class seven : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShoppingCartItems_Cars_PizzaId",
                table: "ShoppingCartItems");

            migrationBuilder.RenameColumn(
                name: "PizzaId",
                table: "ShoppingCartItems",
                newName: "CarId");

            migrationBuilder.RenameIndex(
                name: "IX_ShoppingCartItems_PizzaId",
                table: "ShoppingCartItems",
                newName: "IX_ShoppingCartItems_CarId");

            migrationBuilder.AddForeignKey(
                name: "FK_ShoppingCartItems_Cars_CarId",
                table: "ShoppingCartItems",
                column: "CarId",
                principalTable: "Cars",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShoppingCartItems_Cars_CarId",
                table: "ShoppingCartItems");

            migrationBuilder.RenameColumn(
                name: "CarId",
                table: "ShoppingCartItems",
                newName: "PizzaId");

            migrationBuilder.RenameIndex(
                name: "IX_ShoppingCartItems_CarId",
                table: "ShoppingCartItems",
                newName: "IX_ShoppingCartItems_PizzaId");

            migrationBuilder.AddForeignKey(
                name: "FK_ShoppingCartItems_Cars_PizzaId",
                table: "ShoppingCartItems",
                column: "PizzaId",
                principalTable: "Cars",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
