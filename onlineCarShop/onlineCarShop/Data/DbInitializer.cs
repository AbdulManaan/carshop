﻿using Microsoft.EntityFrameworkCore;
using OnlineCarShop.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCarShop.Data
{
    public class DbInitializer
    {
        public static async Task Initialize(ApplicationDbContext context, 
            IServiceProvider serviceProvider,
           IAdminRepository adminRepository
            )
        {
            context.Database.EnsureCreated();

            if (await context.Cars.AnyAsync())
            {
                return;
            }
            else
            {
                await adminRepository.ClearDatabaseAsync();
                await adminRepository.SeedDatabaseAsync();
            }

        }
    }
}
