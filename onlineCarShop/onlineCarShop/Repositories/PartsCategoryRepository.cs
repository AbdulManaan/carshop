﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OnlineCarShop.Data;
using OnlineCarShop.Models;

namespace OnlineCarShop.Repositories
{
    public class PartsCategoryRepository : IPartsCategoryRepository
    {
        private readonly ApplicationDbContext _context;

        public PartsCategoryRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<PartsCategories> PartsCategories => _context.PartsCategories.Include(x => x.Parts); //include here

        public void Add(PartsCategories PartsCategory)
        {
            _context.Add(PartsCategory);
        }

        public IEnumerable<PartsCategories> GetAll()
        {
            return _context.PartsCategories.ToList();
        }

        public async Task<IEnumerable<PartsCategories>> GetAllAsync()
        {
            return await _context.PartsCategories.ToListAsync();
        }

        public PartsCategories GetById(int? id)
        {
            return _context.PartsCategories.FirstOrDefault(p => p.Id == id);
        }

        public async Task<PartsCategories> GetByIdAsync(int? id)
        {
            return await _context.PartsCategories.FirstOrDefaultAsync(p => p.Id == id);
        }

        public bool Exists(int id)
        {
            return _context.PartsCategories.Any(p => p.Id == id);
        }

        public void Remove(PartsCategories category)
        {
            _context.Remove(category);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Update(PartsCategories category)
        {
            _context.Update(category);
        }

    }
}
