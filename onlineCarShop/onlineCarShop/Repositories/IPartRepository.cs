﻿using OnlineCarShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCarShop.Repositories

{
    public interface IPartRepository
    {
        IEnumerable<Parts> Parts { get; }


        Parts GetById(int? id);
        Task<Parts> GetByIdAsync(int? id);

        Parts GetByIdIncluded(int? id);
        Task<Parts> GetByIdIncludedAsync(int? id);

        bool Exists(int id);

        IEnumerable<Parts> GetAll();
        Task<IEnumerable<Parts>> GetAllAsync();

        IEnumerable<Parts> GetAllIncluded();
        Task<IEnumerable<Parts>> GetAllIncludedAsync();

        void Add(Parts Part);
        void Update(Parts Part);
        void Remove(Parts Part);

        void SaveChanges();
        Task SaveChangesAsync();

    }
}
