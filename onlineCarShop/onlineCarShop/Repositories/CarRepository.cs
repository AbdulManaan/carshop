﻿using Microsoft.EntityFrameworkCore;
using OnlineCarShop.Data;
using OnlineCarShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCarShop.Repositories
{
    public class CarRepository : ICarRepository
    {
        private readonly ApplicationDbContext _context;

        public CarRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Cars> Cars => _context.Cars.Include(p => p.Category).Include(p => p.Reviews); //include here

        public IEnumerable<Cars> CarsOfTheWeek => _context.Cars.Where(p => p.IsMostDemandCar).Include(p => p.Category);

        public void Add(Cars car)
        {
            _context.Add(car);
        }

        public IEnumerable<Cars> GetAll()
        {
            return _context.Cars.ToList();
        }

        public async Task<IEnumerable<Cars>> GetAllAsync()
        {
            return await _context.Cars.ToListAsync();
        }

        public async Task<IEnumerable<Cars>> GetAllIncludedAsync()
        {
            return await _context.Cars.Include(p => p.Category).Include(p => p.Reviews).ToListAsync();
        }

        public IEnumerable<Cars> GetAllIncluded()
        {
            return _context.Cars.Include(p => p.Category).Include(p => p.Reviews).ToList();
        }

        public Cars GetById(int? id)
        {
            return _context.Cars.FirstOrDefault(p => p.Id == id);
        }

        public async Task<Cars> GetByIdAsync(int? id)
        {
            return await _context.Cars.FirstOrDefaultAsync(p => p.Id == id);
        }

        public Cars GetByIdIncluded(int? id)
        {
            return _context.Cars.Include(p => p.Category).Include(p => p.Reviews).FirstOrDefault(p => p.Id == id);
        }

        public async Task<Cars> GetByIdIncludedAsync(int? id)
        {
            return await _context.Cars.Include(p => p.Category).Include(p => p.Reviews).FirstOrDefaultAsync(p => p.Id == id);
        }

        public bool Exists(int id)
        {
            return _context.Cars.Any(p => p.Id == id);
        }

        public void Remove(Cars car)
        {
            _context.Remove(car);
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Update(Cars car)
        {
            _context.Update(car);
        }

    }
}
