﻿using Microsoft.EntityFrameworkCore;
using OnlineCarShop.Data;
using OnlineCarShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCarShop.Repositories
{
    public class PartRepository : IPartRepository
    {
        private readonly ApplicationDbContext _context;

        public PartRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Parts> Parts => _context.Parts.Include(p => p.PartsCategory).Include(p => p.Reviews); //include here

        public void Add(Parts car)
        {
            _context.Add(car);
        }

        public IEnumerable<Parts> GetAll()
        {
            return _context.Parts.ToList();
        }

        public async Task<IEnumerable<Parts>> GetAllAsync()
        {
            return await _context.Parts.ToListAsync();
        }

        public async Task<IEnumerable<Parts>> GetAllIncludedAsync()
        {
            return await _context.Parts.Include(p => p.PartsCategory).Include(p => p.Reviews).ToListAsync();
        }

        public IEnumerable<Parts> GetAllIncluded()
        {
            return _context.Parts.Include(p => p.PartsCategory).Include(p => p.Reviews).ToList();
        }

        public Parts GetById(int? id)
        {
            return _context.Parts.FirstOrDefault(p => p.Id == id);
        }

        public async Task<Parts> GetByIdAsync(int? id)
        {
            return await _context.Parts.FirstOrDefaultAsync(p => p.Id == id);
        }

        public Parts GetByIdIncluded(int? id)
        {
            return _context.Parts.Include(p => p.PartsCategory).Include(p => p.Reviews).FirstOrDefault(p => p.Id == id);
        }

        public async Task<Parts> GetByIdIncludedAsync(int? id)
        {
            return await _context.Parts.Include(p => p.PartsCategory).Include(p => p.Reviews).FirstOrDefaultAsync(p => p.Id == id);
        }

        public bool Exists(int id)
        {
            return _context.Cars.Any(p => p.Id == id);
        }

        public void Remove(Parts car)
        {
            _context.Remove(car);
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Update(Parts car)
        {
            _context.Update(car);
        }

    }
}
