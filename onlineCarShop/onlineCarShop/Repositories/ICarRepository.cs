﻿using OnlineCarShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCarShop.Repositories

{
    public interface ICarRepository
    {
        IEnumerable<Cars> Cars { get; }
        IEnumerable<Cars> CarsOfTheWeek { get; }

        Cars GetById(int? id);
        Task<Cars> GetByIdAsync(int? id);

        Cars GetByIdIncluded(int? id);
        Task<Cars> GetByIdIncludedAsync(int? id);

        bool Exists(int id);

        IEnumerable<Cars> GetAll();
        Task<IEnumerable<Cars>> GetAllAsync();

        IEnumerable<Cars> GetAllIncluded();
        Task<IEnumerable<Cars>> GetAllIncludedAsync();

        void Add(Cars car);
        void Update(Cars car);
        void Remove(Cars car);

        void SaveChanges();
        Task SaveChangesAsync();

    }
}
