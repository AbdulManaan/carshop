﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using OnlineCarShop.Repositories;
using OnlineCarShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineCarShop.Data;

namespace OnlineCarShop.Repositories
{
    public class AdminRepository : IAdminRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IServiceProvider _serviceProvider;
        RoleManager<IdentityRole> _roleManager = null;
        UserManager<ApplicationUser> _userManager = null;
        public AdminRepository(ApplicationDbContext context
            , IServiceProvider serviceProvider
            , RoleManager<IdentityRole> roleManager
            , UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _serviceProvider = serviceProvider;
            _roleManager = roleManager;
            _userManager = userManager;

        }

        public async Task SeedDatabaseAsync()
        {
            

            var cat1 = new Categories { Name = "", Description = "" };
            var cat2 = new Categories { Name = "", Description = "" };
            var cat3 = new Categories { Name = "", Description = "" };

            var cats = new List<Categories>()
            {
                cat1, cat2, cat3
            };

            var car1 = new Cars { Name = "", Price = 70.00M, Category = cat1, Description = "", IsMostDemandCar = false };
            var car2 = new Cars { Name = "", Price = 70.00M, Category = cat3, Description = "", ImageUrl = "", IsMostDemandCar = false };
            var car3 = new Cars { Name = "", Price = 75.00M, Category = cat1, Description = "", ImageUrl = "", IsMostDemandCar = true };
            var car4 = new Cars { Name = "", Price = 65.00M, Category = cat1, Description = "", ImageUrl = "", IsMostDemandCar = false };
            var car5 = new Cars { Name = " ", Price = 85.00M, Category = cat2, Description = "", ImageUrl = "", IsMostDemandCar = true };
            var car6 = new Cars { Name = "", Price = 80.00M, Category = cat1, Description = "", ImageUrl = "", IsMostDemandCar = true };
            var car7 = new Cars { Name = "", Price = 70.00M, Category = cat1, Description = "", ImageUrl = "", IsMostDemandCar = false };
            var car8 = new Cars { Name = " ", Price = 89.00M, Category = cat2, Description = "", ImageUrl = "", IsMostDemandCar = false };
            var car9 = new Cars { Name = "", Price = 69.00M, Category = cat3, Description = "", ImageUrl = "", IsMostDemandCar = false };
            var car10 = new Cars { Name = " ", Price = 75.00M, Category = cat1, Description = "", ImageUrl = "", IsMostDemandCar = false };

            var pizs = new List<Cars>()
            {
                car1, car2, car3, car4, car5, car6, car7, car8, car9, car10
            };

            var user1 = new ApplicationUser { UserName = "user1@gmail.com", Email = "user1@gmail.com" };
            var user2 = new ApplicationUser { UserName = "user2@gmail.com", Email = "user2@gmail.com" };
            var user3 = new ApplicationUser { UserName = "user3@gmail.com", Email = "user3@gmail.com" };
            var user4 = new ApplicationUser { UserName = "user4@gmail.com", Email = "user4@gmail.com" };
            var user5 = new ApplicationUser { UserName = "user5@gmail.com", Email = "user5@gmail.com" };

            string userPassword = "Password123";

            var users = new List<ApplicationUser>()
            {
                user1, user2, user3, user4, user5
            };

            foreach (var user in users)
            {
                await _userManager.CreateAsync(user, userPassword);

            }

     

            await _context.SaveChangesAsync();
        }

        public async Task ClearDatabaseAsync()
        {
           

            var reviews = _context.Reviews.ToList();
            _context.Reviews.RemoveRange(reviews);

            var shoppingCartItems = _context.ShoppingCartItems.ToList();
            _context.ShoppingCartItems.RemoveRange(shoppingCartItems);

            var users = _context.Users.Include(x => x.Roles).ToList();
            foreach (var user in users)
            {
                if (!user.Roles.Any())
                {
                    _context.Users.Remove(user);
                }
            }

           

            var categories = _context.Categories.ToList();
            _context.Categories.RemoveRange(categories);

            await _context.SaveChangesAsync();
        }

    }
}
