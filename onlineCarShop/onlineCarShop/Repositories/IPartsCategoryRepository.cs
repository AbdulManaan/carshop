﻿using OnlineCarShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCarShop.Repositories
{
    public interface IPartsCategoryRepository
    {
        IEnumerable<PartsCategories> PartsCategories { get; }

        PartsCategories GetById(int? id);
        Task<PartsCategories> GetByIdAsync(int? id);

        bool Exists(int id);

        IEnumerable<PartsCategories> GetAll();
        Task<IEnumerable<PartsCategories>> GetAllAsync();

        void Add(PartsCategories category);
        void Update(PartsCategories category);
        void Remove(PartsCategories category);

        void SaveChanges();
        Task SaveChangesAsync();
    }
}
