﻿using OnlineCarShop.Data;
using OnlineCarShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCarShop.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly ShoppingCart _shoppingCart;


        public OrderRepository(ApplicationDbContext context, ShoppingCart shoppingCart)
        {
            _context = context;
            _shoppingCart = shoppingCart;
        }

        public async Task CreateOrderAsync(Order order)
        {
            
            order.OrderPlaced = DateTime.Now;
            decimal totalPrice = 0M;

            var shoppingCartItems = _shoppingCart.ShoppingCartItems;

            foreach (var shoppingCartItem in shoppingCartItems)
            {
                if (shoppingCartItem.CarId!=null)
                {
                    var orderDetail = new OrderDetail()
                    {
                        Amount = shoppingCartItem.Amount,
                        CarId = shoppingCartItem.Car.Id,
                        Order = order,
                        Price = shoppingCartItem.Car.Price,

                    };
                    totalPrice += orderDetail.Price * orderDetail.Amount;
                    _context.OrderDetails.Add(orderDetail);
                }
                if (shoppingCartItem.PartId != null)
                {
                    var orderDetail = new OrderDetail()
                    {
                        Amount = shoppingCartItem.Amount,
                        PartId = shoppingCartItem.Part.Id,
                        Order = order,
                        Price = shoppingCartItem.Part.Price,

                    };
                    totalPrice += orderDetail.Price * orderDetail.Amount;
                    _context.OrderDetails.Add(orderDetail);
                }


            }

            order.OrderTotal = totalPrice;
            _context.Orders.Add(order);

            await _context.SaveChangesAsync();
        }
    }
}
