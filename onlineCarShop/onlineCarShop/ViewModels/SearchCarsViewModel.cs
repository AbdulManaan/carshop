﻿using OnlineCarShop.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCarShop.ViewModels
{
    public class SearchCarsViewModel
    {
        [Required]
        [DisplayName("Serach")]
        public string SearchText { get; set; }

        public IEnumerable<Cars> CarList { get; set; }

    }
}
