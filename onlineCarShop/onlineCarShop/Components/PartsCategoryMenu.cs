﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineCarShop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCarShop.Components
{
    public class PartsCategoryMenu:ViewComponent
    {
        private readonly ApplicationDbContext _context;
        public PartsCategoryMenu(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var categories = await _context.PartsCategories.OrderBy(c => c.Name).ToListAsync();
            return View(categories);
        }
    }
}
