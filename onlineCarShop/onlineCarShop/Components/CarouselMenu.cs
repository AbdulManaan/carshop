﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineCarShop.Data;
using OnlineCarShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCarShop.Components
{
    public class CarouselMenu : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        public CarouselMenu(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var cars = await _context.Cars.Where(x => x.IsMostDemandCar).ToListAsync();
            return View(cars);
        }
    }
}
