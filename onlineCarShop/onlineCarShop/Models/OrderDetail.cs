﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCarShop.Models
{
    public class OrderDetail
    {
        public int OrderDetailId { get; set; }
        public int OrderId { get; set; }
        public int? CarId { get; set; }
        public int? PartId { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
        public virtual Cars Car { get; set; }
        public virtual Parts Part { get; set; }
        public virtual Order Order { get; set; }
    }
}
