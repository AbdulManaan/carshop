﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCarShop.Models
{
    public class ShoppingCartItem
    {
        public int ShoppingCartItemId { get; set; }
        public int? CarId { get; set; }
        public Cars Car { get; set; }
        public int? PartId { get; set; }
        public Parts Part { get; set; }
        public int Amount { get; set; }
        public string ShoppingCartId { get; set; }
    }
}
