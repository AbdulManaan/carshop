﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineCarShop.Data;
using Microsoft.AspNetCore.Authorization;
using OnlineCarShop.Repositories;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineCarShop.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IAdminRepository _adminRepo;

        public AdminController(ApplicationDbContext context, IAdminRepository adminRepo)
        {
            _context = context;
            _adminRepo = adminRepo;
        }

        public async Task<IActionResult> ClearDatabaseAsync()
        {
            await _adminRepo.ClearDatabaseAsync();
            return RedirectToAction("Index", "Cars", null);
        }

        public async Task<IActionResult> SeedDatabaseAsync()
        {
            await _adminRepo.ClearDatabaseAsync();
            await _adminRepo.SeedDatabaseAsync();
            return RedirectToAction("Index", "Cars", null);
        }

    }
}
