using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineCarShop.Repositories;
using OnlineCarShop.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using OnlineCarShop.Data;
using System.Net.Mail;
using Microsoft.AspNetCore.Hosting;

namespace OnlineCarShop.Controllers
{
    [Authorize(Roles = "Admin")]
    [Authorize]
    public class OrdersController : Controller
    {
        private readonly IOrderRepository _orderRepository;
        private readonly ShoppingCart _shoppingCart;
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment env;

        public OrdersController(IOrderRepository orderRepository, 
            ShoppingCart shoppingCart, ApplicationDbContext context, UserManager<ApplicationUser> userManager
            , IHostingEnvironment _env)
        {
            _orderRepository = orderRepository;
            _shoppingCart = shoppingCart;
            _context = context;
            _userManager = userManager;
            env = _env;
        }

        [Authorize]
        public IActionResult Checkout()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Checkout(Order order)
        {
            var userId = _userManager.GetUserId(HttpContext.User);
            order.UserId = userId;

            var items = await _shoppingCart.GetShoppingCartItemsAsync();
            _shoppingCart.ShoppingCartItems = items;

            if (_shoppingCart.ShoppingCartItems.Count == 0)
            {
                ModelState.AddModelError("", "Your cart is empty, add some Items first");
            }

            if (ModelState.IsValid)
            {
                await _orderRepository.CreateOrderAsync(order);
                await _shoppingCart.ClearCartAsync();
                TempData["orderid"] = order.OrderId;
                TempData["ordertotal"] = order.OrderTotal;
                return RedirectToAction("CheckoutComplete");
            }

            return View(order);
        }

        [Authorize]
        public IActionResult CheckoutComplete()
        {
            try
            {
                int orderid = Convert.ToInt32(TempData["orderid"]);
                int ordertotal = Convert.ToInt32(TempData["ordertotal"]);
                var userId = _userManager.GetUserId(HttpContext.User);
                ApplicationUser user=_userManager.Users.FirstOrDefault(u=>u.Id==userId);
             
                string rootPath = env.WebRootPath;
               
                MailMessage EmailObject = new MailMessage();
                EmailObject.From = new MailAddress("onlinecarshop01@gmail.com", " Online Car Shop");
                EmailObject.To.Add(new MailAddress(user.Email));
                EmailObject.Subject = "Order Confirmation";
                EmailObject.Body = "Thanks for your order, We'll deliver your Items soon!<br/>"+ "Your Order Id is:"+ orderid + "<br/>Total Amount is: $"+ordertotal;
                EmailObject.IsBodyHtml = true;
                SmtpClient SC = new SmtpClient("smtp.gmail.com", 587);
                SC.Credentials = new System.Net.NetworkCredential("onlinecarshop01@gmail.com", "Password@123");
                SC.EnableSsl = true;
                SC.Send(EmailObject);

                ViewBag.message = "Check your inbox,Email send at : "+user.Email;

            }
            catch (Exception)
            {
                ViewBag.message = "Email Not sent ,Please Contact with Us on 0300-1234567";
            }
          
            ViewBag.CheckoutCompleteMessage = $"Thanks for your order, We'll deliver your Items soon!";
            return View();
        }

        // GET: Reviews
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            bool isAdmin = await _userManager.IsInRoleAsync(user, "Admin");

            if (isAdmin)
            {
                var allOrders = await _context.Orders.Include(o => o.OrderLines).Include(o => o.User).ToListAsync();
                return View(allOrders);
            }
            else
            {
                var orders = await _context.Orders.Include(o => o.OrderLines).Include(o => o.User)
                    .Where(r => r.User == user).ToListAsync();
                return View(orders);
            }
        }

        // GET: Orders/Details/5
        [Authorize]
        public async Task<IActionResult> Details(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var orders = await _context.Orders.Include(o => o.OrderLines).Include(o => o.User)
                .SingleOrDefaultAsync(m => m.OrderId == id);
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var userRoles = await _userManager.GetRolesAsync(user);
            bool isAdmin = userRoles.Any(r => r == "Admin");

            if (orders == null)
            {
                return NotFound();
            }

            if (isAdmin == false)
            {
                var userId = _userManager.GetUserId(HttpContext.User);
                if (orders.UserId != userId)
                {
                    return BadRequest("You do not have permissions to view this order.");
                }
            }

            var orderDetailsList = _context.OrderDetails.Include(o => o.Car).Include(o => o.Order)
                .Where(x => x.OrderId == orders.OrderId);

            ViewBag.OrderDetailsList = orderDetailsList;

            return View(orders);
        }

        [Authorize(Roles = "Admin")]
        // GET: Orders/Edit/5
        public IActionResult Edit(int id)
        {
            return View();
        }
        [Authorize(Roles = "Admin")]
        // POST: Orders/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Orders/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Orders.Include(o => o.User)
                .SingleOrDefaultAsync(m => m.OrderId == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: OrdersTest/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _context.Orders.SingleOrDefaultAsync(m => m.OrderId == id);
            _context.Orders.Remove(order);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

    }
}