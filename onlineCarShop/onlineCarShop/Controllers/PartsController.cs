using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OnlineCarShop.Models;
using Microsoft.AspNetCore.Authorization;
using OnlineCarShop.ViewModels;
using OnlineCarShop.Data;
using OnlineCarShop.Repositories;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace OnlineCarShop.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PartsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IPartRepository _partRepo;
        private readonly IPartsCategoryRepository _categoryRepo;
        private readonly IHostingEnvironment env = null;
        IServiceProvider services=null;
        public PartsController(ApplicationDbContext context, IPartRepository carRepo
            , IPartsCategoryRepository categoryRepo
            , IHostingEnvironment ev,
            IServiceProvider _services)
        {
            _context = context;
            _partRepo = carRepo;
            _categoryRepo = categoryRepo;
            env = ev;
            services = _services;
        }
        [AllowAnonymous]
        public IActionResult AddToShoppingCart(int Partid) {
          
            String sessionId= HttpContext.Session.GetString("CartId");
            var oldItem = _context.ShoppingCartItems.FirstOrDefault(i => i.PartId == Partid && i.ShoppingCartId ==sessionId);
            ShoppingCartItem SCI = new ShoppingCartItem();
            SCI.Amount = 1;
            Parts  part= _context.Parts.Include(p=>p.PartsCategory).FirstOrDefault(p=>p.Id==Partid);
            if (part != null) {
                if (oldItem == null)
                {
                    SCI.Part = part;
                    SCI.ShoppingCartId = sessionId;
                    _context.Add(SCI);
                    _context.SaveChanges();
                }
                else {
                    oldItem.Amount += 1; 
                    _context.ShoppingCartItems.Update(oldItem);
                    _context.SaveChanges();
                }
            }
          

          
            return RedirectToAction("Index", "ShoppingCart");
        }
        [AllowAnonymous]
        public IActionResult RemoveToShoppingCart(int Partid)
        {

            String sessionId = HttpContext.Session.GetString("CartId");
            var oldItem = _context.ShoppingCartItems.FirstOrDefault(i => i.PartId == Partid && i.ShoppingCartId == sessionId);
            if (oldItem.Amount > 1)
            {
                oldItem.Amount -= 1;
                _context.ShoppingCartItems.Update(oldItem);
                _context.SaveChanges();

            }
            else {
                _context.ShoppingCartItems.Remove(oldItem);
                _context.SaveChanges();

            }


            return RedirectToAction("Index", "ShoppingCart");
        }
        // GET: 
        public async Task<IActionResult> Index()
        {
            return View(await _partRepo.GetAllIncludedAsync());
        }

        // GET: 
        [AllowAnonymous]
        public async Task<IActionResult> ListAll()
        {
            var model = new SearchPartsViewModel()
            {
                PartList = await _partRepo.GetAllIncludedAsync(),
                SearchText = null
            };

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ListAll([Bind("SearchText")] SearchPartsViewModel model)
        {
            var cars = await _partRepo.GetAllIncludedAsync();
            if (model.SearchText == null || model.SearchText == string.Empty)
            {
                model.PartList = cars;
                return View(model);
            }

            var input = model.SearchText.Trim();
            if (input == string.Empty || input == null)
            {
                model.PartList = cars;
                return View(model);
            }
            var search = input.ToLower();

            if (string.IsNullOrEmpty(search))
            {
                model.PartList = cars;
            }
            else
            {
                var carList = _context.Parts.Include(x => x.PartsCategory).Include(x => x.Reviews).OrderBy(x => x.Name)
                     .Where(p =>
                     p.Name.ToLower().Contains(search)
                  || p.Price.ToString("c").ToLower().Contains(search)
                  || p.PartsCategory.Name.ToLower().Contains(search)).ToList();

                if (carList.Any())
                {
                    model.PartList = carList;
                }
                else
                {
                    model.PartList = new List<Parts>();
                }

            }
            return View(model);
        }

        // GET: 
        [AllowAnonymous]
        public async Task<IActionResult> ListCategory(string categoryName)
        {
            bool categoryExtist = _context.PartsCategories.Any(c => c.Name == categoryName);
            if (!categoryExtist)
            {
                return NotFound();
            }

            var category = await _context.PartsCategories.FirstOrDefaultAsync(c => c.Name == categoryName);

            if (category == null)
            {
                return NotFound();
            }

            bool anyCars = await _context.Parts.AnyAsync(x => x.PartsCategory == category);
            if (!anyCars)
            {
                return NotFound($"No Part were found in the category: {categoryName}");
            }

            var cars = _context.Parts.Where(x => x.PartsCategory == category)
                .Include(x => x.PartsCategory).Include(x => x.Reviews);

            ViewBag.CurrentCategory = category.Name;
            return View(cars);
        }

        // GET: /Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cars = await _partRepo.GetByIdIncludedAsync(id);

            if (cars == null)
            {
                return NotFound();
            }

            return View(cars);
        }

        // GET: /Details/5
        [AllowAnonymous]
        public async Task<IActionResult> DisplayDetails(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cars = await _partRepo.GetByIdIncludedAsync(id);

         
            double score;
            if (_context.Reviews.Any(x => x.CarId == id))
            {
                var review = _context.Reviews.Where(x => x.CarId == id);
                score = review.Average(x => x.Grade);
                score = Math.Round(score, 2);
            }
            else
            {
                score = 0;
            }
            ViewBag.AverageReviewScore = score;

            if (cars == null)
            {
                return NotFound();
            }

            return View(cars);
        }

        // GET: 
        [AllowAnonymous]
        public async Task<IActionResult> SearchCars()
        {
            var model = new SearchPartsViewModel()
            {
                PartList = await _partRepo.GetAllIncludedAsync(),
                SearchText = null
            };

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SearchCars([Bind("SearchText")] SearchPartsViewModel model)
        {
            var cars = await _partRepo.GetAllIncludedAsync();
            var search = model.SearchText.ToLower();

            if (string.IsNullOrEmpty(search))
            {
                model.PartList = cars;
            }
            else
            {
                var carList = await _context.Parts.Include(x => x.PartsCategory).Include(x => x.Reviews).OrderBy(x => x.Name)
                    .Where(p =>
                     p.Name.ToLower().Contains(search)
                  || p.Price.ToString("c").ToLower().Contains(search)
                  || p.PartsCategory.Name.ToLower().Contains(search)).ToListAsync();

                if (carList.Any())
                {
                    model.PartList = carList;
                }
                else
                {
                    model.PartList = new List<Parts>();
                }

            }
            return View(model);
        }

        // GET: /Create
        public IActionResult Create()
        {
            ViewData["CategoriesId"] = new SelectList(_categoryRepo.GetAll(), "Id", "Name");
            return View();
        }

        // POST: /Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Price,Description,ImageUrl,PartsCategoriesId")] Parts parts)
        {
            if (ModelState.IsValid)
            {
                string webroorpath = env.WebRootPath;
                string DirPath = webroorpath + "/ImagesPart/";
                string imagefilename = Request.Form.Files["ImageUrl"].FileName;
                string fileext = Path.GetExtension(imagefilename);
                string customfilename = parts.Name + DateTime.Now.ToString("ddMMyyyyhhmmss") + fileext;
                string fullsavepath = @DirPath + customfilename;


                //Add new Image
                if (fileext != null && fileext != "")
                {
                    FileStream FS = new FileStream(fullsavepath, System.IO.FileMode.Create);
                    Request.Form.Files["ImageUrl"].CopyTo(FS);
                    FS.Close();
                    parts.ImageUrl = @"/ImagesPart/" + customfilename;

                }
                else
                {
                    parts.ImageUrl = @"/ImagesPart/" + @"defaultImage.png";

                }

                _partRepo.Add(parts);
                await _partRepo.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["CategoriesId"] = new SelectList(_categoryRepo.GetAll(), "Id", "Name", parts.PartsCategoriesId);
            return View(parts);
        }

        // GET: /Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cars = await _partRepo.GetByIdAsync(id);

            if (cars == null)
            {
                return NotFound();
            }
            ViewData["CategoriesId"] = new SelectList(_categoryRepo.GetAll(), "Id", "Name", cars.PartsCategoriesId);
            return View(cars);
        }

        // POST: /Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, String oldImageUrl, [Bind("Id,Name,Price,Description,ImageUrl,PartsCategoriesId")] Parts part)
        {
            if (id != part.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string webroorpath = env.WebRootPath;
                    string DirPath = webroorpath + "/ImagesPart/";
                    string imagefilename = Request.Form.Files["ImageUrl"].FileName;
                    string fileext = Path.GetExtension(imagefilename);
                    string customfilename = part.Name + DateTime.Now.ToString("ddMMyyyyhhmmss") + fileext;
                    string fullsavepath = @DirPath + customfilename;


                    //Add new Image
                    if (fileext != "")
                    {
                        part.ImageUrl = @"/ImagesPart/" + customfilename;

                        _partRepo.Update(part);
                        _partRepo.SaveChanges();
                        FileStream FS = new FileStream(fullsavepath, System.IO.FileMode.Create);
                        Request.Form.Files["ImageUrl"].CopyTo(FS);
                        FS.Close();

                        if (oldImageUrl != @"/ImagesPart/" + @"defaultImage.png")
                        {
                            FileInfo ImageFile = new FileInfo(webroorpath + @oldImageUrl);
                            ImageFile.Delete();

                        }

                    }
                    else
                    {
                        part.ImageUrl = oldImageUrl;
                    }
                    _partRepo.Update(part);
                    _partRepo.SaveChanges();


                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PartsExists(part.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["CategoriesId"] = new SelectList(_categoryRepo.GetAll(), "Id", "Name", part.PartsCategoriesId);
            return View(part);
        }

        // GET: /Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cars = await _partRepo.GetByIdIncludedAsync(id);

            if (cars == null)
            {
                return NotFound();
            }

            return View(cars);
        }

        // POST: /Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cars = await _partRepo.GetByIdAsync(id);
            _partRepo.Remove(cars);
            string rootPath = env.WebRootPath;
            FileInfo PicFile = new FileInfo(@rootPath + cars.ImageUrl);
            if (PicFile != null && cars.ImageUrl != @"/ImagesPart/" + @"defaultImage.png")
            {
                PicFile.Delete();

            }

            await _partRepo.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PartsExists(int id)
        {
            return _partRepo.Exists(id);
        }
    }
}
