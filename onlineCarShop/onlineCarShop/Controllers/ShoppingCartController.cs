using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineCarShop.Repositories;
using OnlineCarShop.Models;
using OnlineCarShop.ViewModels;
using OnlineCarShop.Data;

namespace OnlineCarShop.Controllers
{
    public class ShoppingCartController : Controller
    {
        private readonly ICarRepository _carRepository;
        private readonly ApplicationDbContext _context;
        private readonly ShoppingCart _shoppingCart;

        public ShoppingCartController(ICarRepository carRepository,
            ShoppingCart shoppingCart, ApplicationDbContext context)
        {
            _carRepository = carRepository;
            _shoppingCart = shoppingCart;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var items = await _shoppingCart.GetShoppingCartItemsAsync();
            _shoppingCart.ShoppingCartItems = items;

            var shoppingCartViewModel = new ShoppingCartViewModel
            {
                ShoppingCart = _shoppingCart,
                ShoppingCartTotal = _shoppingCart.GetShoppingCartTotal()
            };

            return View(shoppingCartViewModel);
        }

        public async Task<IActionResult> AddToShoppingCart(int carId)
        {
            var selectedCar = await _carRepository.GetByIdAsync(carId);

            if (selectedCar != null)
            {
                await _shoppingCart.AddToCartAsync(selectedCar, 1);
            }
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> RemoveFromShoppingCart(int carId)
        {
            var selectedCar = await _carRepository.GetByIdAsync(carId);

            if (selectedCar != null)
            {
                await _shoppingCart.RemoveFromCartAsync(selectedCar);
            }
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> ClearCart()
        {
            await _shoppingCart.ClearCartAsync();

            return RedirectToAction("Index");
        }

    }
}