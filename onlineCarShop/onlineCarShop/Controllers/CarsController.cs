using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OnlineCarShop.Models;
using Microsoft.AspNetCore.Authorization;
using OnlineCarShop.ViewModels;
using OnlineCarShop.Data;
using OnlineCarShop.Repositories;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace OnlineCarShop.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CarsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ICarRepository _carRepo;
        private readonly ICategoryRepository _categoryRepo;
        private readonly IHostingEnvironment env = null;
        public CarsController(ApplicationDbContext context, ICarRepository carRepo
            , ICategoryRepository categoryRepo
            ,IHostingEnvironment ev)
        {
            _context = context;
            _carRepo = carRepo;
            _categoryRepo = categoryRepo;
            env = ev;
        }

        // GET: 
        public async Task<IActionResult> Index()
        {
            return View(await _carRepo.GetAllIncludedAsync());
        }

        // GET: 
        [AllowAnonymous]
        public async Task<IActionResult> ListAll()
        {
            var model = new SearchCarsViewModel()
            {
                CarList = await _carRepo.GetAllIncludedAsync(),
                SearchText = null
            };

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ListAll([Bind("SearchText")] SearchCarsViewModel model)
        {
            var cars = await _carRepo.GetAllIncludedAsync();
            if (model.SearchText == null || model.SearchText == string.Empty)
            {
                model.CarList = cars;
                return View(model);
            }

            var input = model.SearchText.Trim();
            if (input == string.Empty || input == null)
            {
                model.CarList = cars;
                return View(model);
            }
            var search = input.ToLower();

            if (string.IsNullOrEmpty(search))
            {
                model.CarList = cars;
            }
            else
            {
                var carList =  _context.Cars.Include(x => x.Category).Include(x => x.Reviews).OrderBy(x => x.Name)
                     .Where(p =>
                     p.Name.ToLower().Contains(search)
                  || p.Price.ToString("c").ToLower().Contains(search)
                  || p.Category.Name.ToLower().Contains(search)).ToList();

                if (carList.Any())
                {
                    model.CarList = carList;
                }
                else
                {
                    model.CarList = new List<Cars>();
                }

            }
            return View(model);
        }

        // GET: 
        [AllowAnonymous]
        public async Task<IActionResult> ListCategory(string categoryName)
        {
            bool categoryExtist = _context.Categories.Any(c => c.Name == categoryName);
            if (!categoryExtist)
            {
                return NotFound();
            }

            var category = await _context.Categories.FirstOrDefaultAsync(c => c.Name == categoryName);

            if (category == null)
            {
                return NotFound();
            }

            bool anyCars = await _context.Cars.AnyAsync(x => x.Category == category);
            if (!anyCars)
            {
                return NotFound($"No Car were found in the category: {categoryName}");
            }

            var cars = _context.Cars.Where(x => x.Category == category)
                .Include(x => x.Category).Include(x => x.Reviews);

            ViewBag.CurrentCategory = category.Name;
            return View(cars);
        }

        // GET: /Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cars = await _carRepo.GetByIdIncludedAsync(id);

            if (cars == null)
            {
                return NotFound();
            }

            return View(cars);
        }

        // GET: /Details/5
        [AllowAnonymous]
        public async Task<IActionResult> DisplayDetails(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cars = await _carRepo.GetByIdIncludedAsync(id);

            //var listOfParts = await _context.CarParts.Where(x => x.CarId == id).Select(x => x.Part.Name).ToListAsync();
            //ViewBag.CarParts = listOfParts;

            //var listOfReviews = await _context.Reviews.Where(x => x.CarId == id).Select(x => x).ToListAsync();
            //ViewBag.Reviews = listOfReviews;
            double score;
            if (_context.Reviews.Any(x => x.CarId == id))
            {
                var review = _context.Reviews.Where(x => x.CarId == id);
                score = review.Average(x => x.Grade);
                score = Math.Round(score, 2);
            }
            else
            {
                score = 0;
            }
            ViewBag.AverageReviewScore = score;

            if (cars == null)
            {
                return NotFound();
            }

            return View(cars);
        }

        // GET: 
        [AllowAnonymous]
        public async Task<IActionResult> SearchCars()
        {
            var model = new SearchCarsViewModel()
            {
                CarList = await _carRepo.GetAllIncludedAsync(),
                SearchText = null
            };

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SearchCars([Bind("SearchText")] SearchCarsViewModel model)
        {
            var cars = await _carRepo.GetAllIncludedAsync();
            var search = model.SearchText.ToLower();

            if (string.IsNullOrEmpty(search))
            {
                model.CarList = cars;
            }
            else
            {
                var carList = await _context.Cars.Include(x => x.Category).Include(x => x.Reviews).OrderBy(x => x.Name)
                    .Where(p =>
                     p.Name.ToLower().Contains(search)
                  || p.Price.ToString("c").ToLower().Contains(search)
                  || p.Category.Name.ToLower().Contains(search)).ToListAsync();

                if (carList.Any())
                {
                    model.CarList = carList;
                }
                else
                {
                    model.CarList = new List<Cars>();
                }

            }
            return View(model);
        }

        // GET: /Create
        public IActionResult Create()
        {
            ViewData["CategoriesId"] = new SelectList(_categoryRepo.GetAll(), "Id", "Name");
            return View();
        }

        // POST: /Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Price,Description,ImageUrl,IsMostDemandCar,CategoriesId")] Cars cars)
        {
            if (ModelState.IsValid)
            {
                string webroorpath = env.WebRootPath;
                string DirPath = webroorpath + "/ImagesCar/";
                string imagefilename = Request.Form.Files["ImageUrl"].FileName;
                string fileext = Path.GetExtension(imagefilename);
                string customfilename = cars.Name + DateTime.Now.ToString("ddMMyyyyhhmmss") + fileext;
                string fullsavepath = @DirPath + customfilename;

               
                //Add new Image
                if (fileext != null && fileext != "")
                {
                    FileStream FS = new FileStream(fullsavepath, System.IO.FileMode.Create);
                    Request.Form.Files["ImageUrl"].CopyTo(FS);
                    FS.Close();
                    cars.ImageUrl = @"/ImagesCar/" + customfilename;
                 
                }
                else
                {
                    cars.ImageUrl = @"/ImagesCar/" + @"defaultImage.png";

                }
             
                _carRepo.Add(cars);
                await _carRepo.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["CategoriesId"] = new SelectList(_categoryRepo.GetAll(), "Id", "Name", cars.CategoriesId);
            return View(cars);
        }

        // GET: /Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cars = await _carRepo.GetByIdAsync(id);

            if (cars == null)
            {
                return NotFound();
            }
            ViewData["CategoriesId"] = new SelectList(_categoryRepo.GetAll(), "Id", "Name", cars.CategoriesId);
            return View(cars);
        }

        // POST: /Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, String oldImageUrl, [Bind("Id,Name,Price,Description,ImageUrl,IsMostDemandCar,CategoriesId")] Cars car)
        {
            if (id != car.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string webroorpath = env.WebRootPath;
                    string DirPath = webroorpath + "/ImagesCar/";
                    string imagefilename = Request.Form.Files["ImageUrl"].FileName;
                    string fileext = Path.GetExtension(imagefilename);
                    string customfilename = car.Name + DateTime.Now.ToString("ddMMyyyyhhmmss") + fileext;
                    string fullsavepath = @DirPath + customfilename;

                   
                    //Add new Image
                    if (fileext != "")
                    {
                        car.ImageUrl = @"/ImagesCar/" + customfilename;
                        
                        _carRepo.Update(car);
                        _carRepo.SaveChanges();
                        FileStream FS = new FileStream(fullsavepath, System.IO.FileMode.Create);
                        Request.Form.Files["ImageUrl"].CopyTo(FS);
                        FS.Close();

                        if(oldImageUrl!= @"/ImagesCar/" + @"defaultImage.png")
                        {
                            FileInfo ImageFile = new FileInfo(webroorpath + @oldImageUrl);
                            ImageFile.Delete();

                        }

                    }
                    else
                    {
                        car.ImageUrl = oldImageUrl;
                    }
                    _carRepo.Update(car);
                    _carRepo.SaveChanges();
                   

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CarsExists(car.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["CategoriesId"] = new SelectList(_categoryRepo.GetAll(), "Id", "Name", car.CategoriesId);
            return View(car);
        }

        // GET: /Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cars = await _carRepo.GetByIdIncludedAsync(id);

            if (cars == null)
            {
                return NotFound();
            }

            return View(cars);
        }

        // POST: /Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cars = await _carRepo.GetByIdAsync(id);
            _carRepo.Remove(cars);
            string rootPath = env.WebRootPath;
            FileInfo PicFile = new FileInfo(@rootPath + cars.ImageUrl);
            if (PicFile != null&&cars.ImageUrl != @"/ImagesCar/" + @"defaultImage.png")
            {
                PicFile.Delete();

            }
          
            await _carRepo.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool CarsExists(int id)
        {
            return _carRepo.Exists(id);
        }
    }
}
