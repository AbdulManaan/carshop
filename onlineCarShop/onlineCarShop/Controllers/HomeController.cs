﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineCarShop.Data;
using Microsoft.AspNetCore.Hosting;
using System.Net.Mail;

namespace OnlineCarShop.Controllers
{
    public class HomeController : Controller
    {
       private ApplicationDbContext _context = null;
        private IHostingEnvironment env = null;
        public HomeController(ApplicationDbContext context,IHostingEnvironment _env) {
            _context = context;
            env = _env;
        }
        public IActionResult Index()
        {
          
            var cars =  _context.Cars.Where(x => x.IsMostDemandCar).ToList();
            return View(cars);
           
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
